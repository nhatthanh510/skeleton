import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { thunkLoadData } from 'store/tree/thunk';

function Test() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(thunkLoadData());
  }, [dispatch]);
  return <div>ahihi</div>;
}

export default Test;
