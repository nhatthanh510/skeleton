import { treeService } from 'services';
import { LOAD_DATA } from 'store/tree/type';
import {
  loadDataAction,
  loadDataSuccessAction,
  loadDataErrorAction,
} from 'store/tree/action';
export const thunkLoadData = () => async dispatch => {
  dispatch(loadDataAction());
  const result = await treeService.loadTreeData();

  if (result) {
    console.log('result', result);
    dispatch(loadDataSuccessAction(result));
  }

  // return loadDataSuccessAction(result);
};
