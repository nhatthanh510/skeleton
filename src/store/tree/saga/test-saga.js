import { all, put, take, select } from 'redux-saga/effects';
import produce from 'immer';
import { getTreeData } from 'store/tree/selector';
import {
  loadDataAction,
  loadDataSuccessAction,
  loadDataErrorAction,
} from 'store/tree/action';

function* loadDataSaga() {
  while (1) {
    const { type } = loadDataAction();
    const action = yield take(type);
    try {
      yield put(loadDataSuccessAction(action.payload));
    } catch (err) {
      yield put(loadDataErrorAction(err));
    }
  }
}

export default function* rootSaga() {
  yield all([loadDataSaga()]);
}
