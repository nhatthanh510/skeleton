import { all } from 'redux-saga/effects';

import testSaga from './test-saga';
import test2Saga from './test2-saga';

export default function* rootSaga() {
  yield all([testSaga(), test2Saga()]);
}
