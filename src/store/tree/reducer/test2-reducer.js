import update from 'immutability-helper';

import { handleActions } from 'redux-actions';

import { initialState } from './initialize-state';
import { LOAD_DATA, LOAD_DATA_SUCCESS, LOAD_DATA_ERROR } from '../type';

const treeItemReducer = handleActions(
  // @ts-ignore
  new Map([
    [
      LOAD_DATA,
      (state, action) => {
        return update(state, {
          loading: { $set: true },
        });
      },
    ],
    [
      LOAD_DATA_SUCCESS,
      (state, action) => {
        return update(state, {
          loading: { $set: true },
          data: { $set: action.payload },
        });
      },
    ],
    [
      LOAD_DATA_ERROR,
      (state, action) => {
        return update(state, {
          loading: { $set: false },
        });
      },
    ],
  ]),
  initialState,
);
export default treeItemReducer;
