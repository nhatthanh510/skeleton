import reduceReducers from 'reduce-reducers';

//Siblings reducers
import testReducer from './test-reducer';
import test2Reducer from './test2-reducer';

export default reduceReducers(testReducer, test2Reducer);
