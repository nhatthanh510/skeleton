import update from 'immutability-helper';

import { handleActions } from 'redux-actions';
import { initialState } from './initialize-state';
import {} from '../type';

const treeReducer = handleActions(
  // @ts-ignore
  new Map([]),
  initialState,
);
export default treeReducer;
