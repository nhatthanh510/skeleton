import { createAction } from 'redux-actions';
import { LOAD_DATA, LOAD_DATA_SUCCESS, LOAD_DATA_ERROR } from 'store/tree/type';

//Loading initial data
export const loadDataAction = createAction(LOAD_DATA);
export const loadDataSuccessAction = createAction(LOAD_DATA_SUCCESS);
export const loadDataErrorAction = createAction(LOAD_DATA_ERROR);
